{{/* vim: set filetype=mustache: */}}

{{/*
Return the proper code-server image name
*/}}
{{- define "code-server.image" -}}
{{ include "common.images.image" (dict "imageRoot" .Values.image "global" .Values.global) }}
{{- end -}}

{{/*
Return the proper Docker Image Registry Secret Names
*/}}
{{- define "code-server.imagePullSecrets" -}}
{{- include "common.images.pullSecrets" (dict "images" .Values.image "global" .Values.global) -}}
{{- end -}}

{{/*
Returns the proper service account name depending if an explicit service account name is set
in the values file. If the name is not set it will default to either common.names.fullname if serviceAccount.create
is true or default otherwise.
*/}}
{{- define "code-server.serviceAccountName" -}}
    {{- if .Values.serviceAccount.create -}}
        {{ default (include "common.names.fullname" .) .Values.serviceAccount.name }}
    {{- else -}}
        {{ default "default" .Values.serviceAccount.name }}
    {{- end -}}
{{- end -}}

{{/*
Return true if a secret object should be created
*/}}
{{- define "code-server.createSecret" -}}
{{- if not (or .Values.global.auth.existingSecret .Values.auth.existingSecret) -}}
    {{- true -}}
{{- end -}}
{{- end -}}

{{/*
Get the password secret.
*/}}
{{- define "code-server.secretName" -}}
{{- if .Values.global.auth.existingSecret }}
    {{- printf "%s" (tpl .Values.global.auth.existingSecret $) -}}
{{- else if .Values.auth.existingSecret -}}
    {{- printf "%s" (tpl .Values.auth.existingSecret $) -}}
{{- else -}}
    {{- printf "%s" (include "common.names.fullname" .) -}}
{{- end -}}
{{- end -}}

{{/*
Get the admin-password key.
*/}}
{{- define "code-server.passwordKey" -}}
{{- if or .Values.global.auth.existingSecret .Values.auth.existingSecret }}
    {{- if .Values.global.auth.secretKeys.passwordKey }}
        {{- printf "%s" (tpl .Values.global.auth.secretKeys.passwordKey $) -}}
    {{- else if .Values.auth.secretKeys.passwordKey -}}
        {{- printf "%s" (tpl .Values.auth.secretKeys.passwordKey $) -}}
    {{- end -}}
{{- else -}}
    {{- "password" -}}
{{- end -}}
{{- end -}}