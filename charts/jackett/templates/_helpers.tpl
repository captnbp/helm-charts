{{/* vim: set filetype=mustache: */}}

{{/*
Return the proper jackett image name
*/}}
{{- define "jackett.image" -}}
{{ include "common.images.image" (dict "imageRoot" .Values.image "global" .Values.global) }}
{{- end -}}

{{/*
Return the proper flaresolverr image name
*/}}
{{- define "flaresolverr.image" -}}
{{ include "common.images.image" (dict "imageRoot" .Values.flaresolverr.image "global" .Values.global) }}
{{- end -}}

{{/*
Return the proper Docker Image Registry Secret Names
*/}}
{{- define "jackett.imagePullSecrets" -}}
{{- include "common.images.pullSecrets" (dict "images" .Values.image "global" .Values.global) -}}
{{- end -}}

{{/*
Return the proper Docker Image Registry Secret Names
*/}}
{{- define "flaresolverr.imagePullSecrets" -}}
{{- include "common.images.pullSecrets" (dict "images" .Values.flaresolverr.image "global" .Values.global) -}}
{{- end -}}

{{/*
Returns the proper service account name depending if an explicit service account name is set
in the values file. If the name is not set it will default to either common.names.fullname if serviceAccount.create
is true or default otherwise.
*/}}
{{- define "jackett.serviceAccountName" -}}
    {{- if .Values.serviceAccount.create -}}
        {{ default (include "common.names.fullname" .) .Values.serviceAccount.name }}
    {{- else -}}
        {{ default "default" .Values.serviceAccount.name }}
    {{- end -}}
{{- end -}}
